#!/usr/bin/env bash

MORELLO_TAGS="tags"

mkdir -p $MORELLO_TAGS

for f in $1/*.img
do
	case ${f} in
		*2[0-9][0-9][0-9][0-1][0-9][0-3][0-9]* )
		continue
		;;
	*)
		filename=$(basename -- "$f")
		filename="${filename%.*}"

		echo Generate: "$MORELLO_TAGS"/"$filename"_"$(date -r "$f" +"%Y%m%d").img"

		cp -n "$f" "$MORELLO_TAGS"/"$filename"_"$(date -r "$f" +"%Y%m%d").img"
	;;
	esac
done
