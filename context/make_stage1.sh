#!/usr/bin/env bash

# SPDX-License-Identifier: BSD-3-Clause

# Build morello-pcuabi-env project
MORELLO_STAGE1_HOME=/morello_sw/stage1

mkdir -p $MORELLO_STAGE1_HOME

MORELLO_HOME=$MORELLO_STAGE1_HOME/morello-pcuabi-env/morello
MORELLO_PROJECTS=$MORELLO_HOME/projects

# Make sure we are in MORELLO_STAGE1_HOME
cd $MORELLO_STAGE1_HOME

# Clone morello-pcuabi-env
git clone https://git.morello-project.org/morello/morello-pcuabi-env.git -b morello/mainline

cd $MORELLO_HOME

# Detect Architecture
_MORELLO_ARCH=$(uname -m)
MORELLO_ARCH="--aarch64"

if [ "${_MORELLO_ARCH}" == 'x86_64' ]; then
	MORELLO_ARCH="--x86_64"
elif [ "${_MORELLO_ARCH}" == 'aarch64' ]; then
	MORELLO_ARCH="--aarch64"
else
	echo "Architecture not supported!"
fi

# Build morello-pcuabi-env
source $MORELLO_HOME/env/morello-pcuabi-env
$MORELLO_HOME/scripts/build-all.sh \
	${MORELLO_ARCH}\
	--firmware \
	--linux
source $MORELLO_HOME/env/morello-pcuabi-env-restore
