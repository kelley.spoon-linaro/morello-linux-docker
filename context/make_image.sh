#!/usr/bin/env bash

# SPDX-License-Identifier: BSD-3-Clause

set -x
set -e

# Stage 1
/morello_sw/scripts/make_stage1.sh

# Stage 2
mkdir -p /morello_sw/debian_bullseye
cd /morello_sw
debootstrap --arch=arm64 --foreign bullseye ./debian_bullseye
cp -av /usr/bin/qemu-aarch64-static ./debian_bullseye/usr/bin
cp -av /etc/resolv.conf ./debian_bullseye/etc/resolv.conf

cp -av /morello_sw/scripts/build_debian_image.sh ./debian_bullseye/
chroot ./debian_bullseye /bin/bash -c ./build_debian_image.sh

rm ./debian_bullseye/usr/bin/qemu-aarch64-static
rm ./debian_bullseye/etc/resolv.conf
rm ./debian_bullseye/build_debian_image.sh

# Move morello-pcuabi-env outside of the rootfs
mkdir -p /morello_sw/stage2
mv /morello_sw/debian_bullseye/morello-pcuabi-env /morello_sw/stage2

# Create Debian Image
dd if=/dev/zero of=morello-debian.img bs=1024K count=4096

__loop_device=$(losetup -f --show morello-debian.img)
mkfs.ext4 $__loop_device
mkdir mnt_point
mount $__loop_device ./mnt_point

rsync -arxHAX --progress ./debian_bullseye/* ./mnt_point/
sync

umount ./mnt_point
losetup --detach $__loop_device
rm -r ./mnt_point

# Create PCuABI busybox image
MORELLO_HOME=/morello_sw/stage2/morello-pcuabi-env/morello
MORELLO_PCUABI_ENV=$MORELLO_HOME/morello-pcuabi-env
MORELLO_DOCKER=$MORELLO_HOME/morello-docker

mkdir -p $MORELLO_PCUABI_ENV
cd $MORELLO_PCUABI_ENV

dd if=/dev/zero of=morello-pcuabi-env.img bs=1024K count=100

__loop_device=$(losetup -f --show morello-pcuabi-env.img)
mkfs.ext4 $__loop_device
mkdir mnt_point
mount $__loop_device ./mnt_point

cd mnt_point
tar -xJvf $MORELLO_DOCKER/morello-busybox-docker.tar.xz
sync
cd ..

umount ./mnt_point
losetup --detach $__loop_device
rm -r ./mnt_point
